jest.mock('../../../lambda/verifyJwt', () => ({
  verifyJwt: jest.fn().mockReturnValue({ id: 'Arbitrary ID' }),
}));

import { withSession } from '../../../lambda/middleware/withSession';

describe('The withSession middleware', () => {
  it('should add the verified session to the middleware context', () => {
    const mockVerifyJwt = require.requireMock('../../../lambda/verifyJwt').verifyJwt;
    mockVerifyJwt.mockReturnValueOnce('Some session');

    const enhancedMiddleware = withSession(checkAssertions);
    enhancedMiddleware('Arbitrary context');

    function checkAssertions(enhancedContext): any {
      expect(enhancedContext.session).toBe('Some session');
    }
  });

  it('should not add a session context when no HTTP headers were present', () => {
    const mockVerifyJwt = require.requireMock('../../../lambda/verifyJwt').verifyJwt;

    const enhancedMiddleware = withSession(checkAssertions);
    enhancedMiddleware('Arbitrary context');

    function checkAssertions(enhancedContext): any {
      expect(mockVerifyJwt.mock.calls[0][0]).toBeUndefined();
    }
  });

  it('should not add a session context when no Authorization HTTP headers were present', () => {
    const mockVerifyJwt = require.requireMock('../../../lambda/verifyJwt').verifyJwt;

    const enhancedMiddleware = withSession(checkAssertions);
    enhancedMiddleware({ headers: { 'Non-Authorization header': 'Arbitary value' } });

    function checkAssertions(enhancedContext): any {
      expect(mockVerifyJwt.mock.calls[0][0]).toBeUndefined();
    }
  });

  it('should not add a session context when the Authorization header does not include a bearer token', () => {
    const mockVerifyJwt = require.requireMock('../../../lambda/verifyJwt').verifyJwt;

    const enhancedMiddleware = withSession(checkAssertions);
    enhancedMiddleware({ headers: { authorization: 'Basic non-jwt_token' } });

    function checkAssertions(enhancedContext): any {
      expect(mockVerifyJwt.mock.calls[0][0]).toBeUndefined();
    }
  });

  it('should read the JSON Web Token from the request headers if present', () => {
    const mockVerifyJwt = require.requireMock('../../../lambda/verifyJwt').verifyJwt;

    const enhancedMiddleware = withSession(jest.fn());
    enhancedMiddleware({ headers: { authorization: 'Bearer some_token' } });

    expect(mockVerifyJwt.mock.calls.length).toBe(1);
    expect(mockVerifyJwt.mock.calls[0][0]).toBe('some_token');
  });

  it('should read the JSON Web Token regardless of the Authorization header\'s capitalisation', () => {
    const mockVerifyJwt = require.requireMock('../../../lambda/verifyJwt').verifyJwt;

    const enhancedMiddleware = withSession(jest.fn());
    enhancedMiddleware({ headers: { aUtHoRiZaTiOn: 'Bearer some_token' } });

    expect(mockVerifyJwt.mock.calls.length).toBe(1);
    expect(mockVerifyJwt.mock.calls[0][0]).toBe('some_token');
  });
});
