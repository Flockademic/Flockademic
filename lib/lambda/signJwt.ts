import { sign } from 'jsonwebtoken';

import { Session } from '../interfaces/Session';

export function signJwt(data: Session): string | Error {
  const jwtSecret = process.env.jwt_secret;
  if (!jwtSecret) {
    // tslint:disable-next-line:no-console
    console.log('Deployment error: no JSON Web Token secret was defined in $TF_VAR_jwt_secret');

    return new Error('We are currently experiencing issues, please try again later.');
  }

  return sign(data, jwtSecret, { algorithm: 'HS256', expiresIn: 20 * 60 });
}
