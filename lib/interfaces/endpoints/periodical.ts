import { InitialisedPeriodical, Periodical } from '../Periodical';
import { Person } from '../Person';
import { InitialisedScholarlyArticle, PublishedScholarlyArticle, ScholarlyArticle } from '../ScholarlyArticle';

export type GetPeriodicalResponse = Partial<Periodical>;

export type GetPeriodicalsResponse = InitialisedPeriodical[];

export interface GetPeriodicalContentsResponse extends Partial<Periodical> {
  hasPart: Array<Partial<ScholarlyArticle>>;
}

export interface GetDashboardResponse {
  articles: null | InitialisedScholarlyArticle[];
  periodicals: null | InitialisedPeriodical[];
}

// A FindAction that either could not find anything (error), or found a periodical
export interface GetIsPeriodicalSlugAvailableResponse {
  error?: { description: string; };
  result?: { identifier: string; };
}

export interface PostCreatePeriodicalRequest {
  result?: Partial<Periodical>;
}
export interface NewPeriodical extends Partial<Periodical> {
  identifier: string;
}
export interface PostCreatePeriodicalResponse {
  result: NewPeriodical;
}
export interface PostUpdatePeriodicalRequest {
  object: Partial<Periodical>;
  targetCollection: {
    identifier: string;
  };
}
export interface PostUpdatePeriodicalResponse {
  result: Partial<Periodical>;
  targetCollection: {
    identifier: string;
  };
}

export interface PostGenerateUploadUrlForPeriodicalAssetsRequest {
  result: {
    image: string;
  };
  targetCollection: {
    identifier: string;
  };
}

// A schema.org AllocateAction, the object being a DownloadAction (there is no UploadAction),
// and the result being a Periodical with a file attached.
export interface PostGenerateUploadUrlForPeriodicalAssetsResponse {
  object: {
    toLocation: string;
  };
  result: {
    image: string;
  };
}

export interface PostMakePeriodicalPublicRequest {
  object: {
    identifier: string;
  };
  targetCollection: {
    identifier: string;
  };
}
export interface PostMakePeriodicalPublicResponse {
  result: {
    creator: Person,
    identifier: string;
  };
  targetCollection: {
    identifier: string;
  };
}

export type GetScholarlyArticleResponse = Partial<ScholarlyArticle>;

export type GetPublishedScholarlyArticlesResponse = PublishedScholarlyArticle[];
export type GetScholarlyArticlesResponse = Array<Partial<ScholarlyArticle>>;

export interface PostInitialiseScholarlyArticleRequest {
  result: Partial<ScholarlyArticle> & { isPartOf?: Partial<Periodical> & { identifier: string; } };
}
export interface NewScholarlyArticle extends Partial<ScholarlyArticle> {
  identifier: string;
}
export interface PostInitialiseScholarlyArticleResponse {
  result: NewScholarlyArticle;
}

export interface PostUpdateScholarlyArticleRequest {
  object: Partial<ScholarlyArticle>;
  targetCollection: {
    identifier: string;
  };
}
export interface PostUpdateScholarlyArticleResponse {
  result: Partial<ScholarlyArticle>;
  targetCollection: {
    identifier: string;
  };
}

export interface PostGenerateUploadUrlForScholarlyArticleRequest {
  result: {
    associatedMedia: {
      license: 'https://creativecommons.org/licenses/by/4.0/';
      name: string;
    };
  };
  targetCollection: {
    identifier: string;
  };
}

// A schema.org AllocateAction, the object being a DownloadAction (there is no UploadAction),
// and the result being a ScholarlyArticle with a file attached.
export interface PostGenerateUploadUrlForScholarlyArticleResponse {
  object: {
    toLocation: string;
  };
  result: {
    associatedMedia: {
      license: 'https://creativecommons.org/licenses/by/4.0/';
      name: string;
      contentUrl: string;
    };
  };
}

export interface PostPublishScholarlyArticleRequest {
  targetCollection: {
    identifier: string;
  };
}
export interface PostPublishScholarlyArticleResponse {
  result: {
    author: {
      identifier: string;
    };
    datePublished: string;
    identifier: string;
  };
  targetCollection: {
    identifier: string;
  };
}

export interface PostSubmitScholarlyArticleRequest {
  result: {
    isPartOf: {
      identifier: string;
    };
  };
  targetCollection: {
    identifier: string;
  };
}
export interface PostSubmitScholarlyArticleResponse {
  result: {
    author: {
      identifier: string;
    };
    datePublished: string;
    identifier: string;
    isPartOf: {
      identifier: string;
    };
  };
  targetCollection: {
    identifier: string;
  };
}
