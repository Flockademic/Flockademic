import * as React from 'react';
import { OutboundLink } from 'react-ga';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';

import { AccountState } from '../../ducks/account';
import { AppState } from '../../ducks/app';
import { Spinner } from '../spinner/component';

interface OrcidButtonState { }
export interface OrcidButtonDispatchProps {}
export interface OrcidButtonStateProps {
  state: AccountState;
}
export interface OrcidButtonOwnProps {
  className?: string;
  pendingElement?: JSX.Element;
  redirectPath?: string;
  title?: string;
}

export type OrcidButtonProps = OrcidButtonOwnProps & OrcidButtonStateProps & OrcidButtonDispatchProps;

export class BareOrcidButton extends React.Component <
  OrcidButtonProps & RouteComponentProps<{}>
  , OrcidButtonState
> {
  public state: OrcidButtonState = { verifying: false };

  public render() {
    if (this.props.state.verifying) {
      return this.props.pendingElement || <Spinner size="small"/>;
    }

    if (this.props.state.orcid) {
      return (
        <Link
          to={`/profile/${this.props.state.orcid}`}
          title="View your profile"
          className="button is-text"
        >
          Your profile
        </Link>
      );
    }

    return (
      <OutboundLink
        to={this.getOAuthUrl()}
        eventLabel="orcid-signin"
        title={this.props.title || 'Create account / sign in'}
        className={this.props.className}
      >
        {this.props.children}
      </OutboundLink>
    );
  }

  private getRedirectUri() {
    // I'm ashamed of the next line: since I couldn't find out how to modify document.location in tests,
    // I've modified the code here to ignore it if it's undefined - in the browser, we can count on
    // it being set anyway.
    /* istanbul ignore next */
    const baseUrl = (typeof document !== 'undefined' && document.location) ?
      `${document.location.protocol}//${document.location.host}`
      : 'https://flockademic.com';

    const redirectPath = this.props.redirectPath || (this.props.location.pathname + this.props.location.hash);

    return baseUrl + redirectPath;
  }

  private getOAuthUrl() {
    const redirectUri = this.getRedirectUri();

    return `${process.env.ORCID_BASE_PATH}/oauth/authorize`
  + `?client_id=${process.env.ORCID_CLIENT_ID}`
  + '&response_type=code'
  + '&scope=/authenticate'
  + `&redirect_uri=${encodeURI(redirectUri)}`;
  }
}

// The following two functions are trivial (and forced to be correct through the types),
// but testing them requires inspecting the props of the connected component and mocking the store.
// Figuring that out is not worth it, so we just don't test this:
/* istanbul ignore next */
function mapStateToOrcidButtonProps(state: AppState): OrcidButtonStateProps {
  return { state: state.account };
}
const ConnectedButton = connect<OrcidButtonStateProps, OrcidButtonDispatchProps, OrcidButtonOwnProps, AppState>(
  mapStateToOrcidButtonProps,
)(BareOrcidButton);

export const OrcidButton = withRouter<RouteComponentProps<{}> & OrcidButtonOwnProps>(ConnectedButton);
