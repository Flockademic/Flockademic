import { MaintenanceModePage } from '../../src/components/maintenanceModePage/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should match the snapshot', () => {
  const quip = shallow(<MaintenanceModePage/>);

  expect(toJson(quip)).toMatchSnapshot();
});
