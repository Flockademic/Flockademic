jest.mock('react-ga');
jest.mock('../../src/services/periodical', () => ({
  getScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve({})),
  publishScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve(
    {
      result: { datePublished: 'arbitrary date' },
    },
  )),
  submitScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve(
    {
      result: { datePublished: 'arbitrary date' },
    },
  )),
  updateScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve(
    {
      name: 'Arbitrary name',
    },
  )),
}));

import {
  ArticleManagementPageProps,
  ArticleManagementPageRouteParams,
  BareArticleManagementPage,
} from '../../src/components/articleManagementPage/component';
import { Spinner } from '../../src/components/spinner/component';
import { mockRouterContext } from '../mockRouterContext';

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';
import { MemoryRouter, Redirect } from 'react-router';

const mockScholarlyArticle = {
  associatedMedia: {
    contentUrl: 'arbitrary_url',
    name: 'arbitrary_filename',
  },
  description: 'Arbitrary abstract',
  isPartOf: {
    identifier: 'Arbitrary journal ID',
  },
  name: 'Arbitrary title',
};

const mockProps: ArticleManagementPageProps = {
};
function initialisePage({
  articleId = 'arbitrary_id',
  orcid = mockProps.orcid,
  onUpdate = jest.fn(),
}: Partial<ArticleManagementPageProps & ArticleManagementPageRouteParams> = {}) {
  return mount(
    <BareArticleManagementPage match={{ params: { articleId }, url: 'https://arbitrary_url' }} orcid={orcid} onUpdate={onUpdate}/>,
    mockRouterContext,
  );
}

it('should display a spinner while the article details are loading', () => {
  const page = initialisePage();

  expect(page.find(Spinner)).toExist();
});

it('should display an error when the article\'s details could not be loaded', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(toJson(page.find(BareArticleManagementPage))).toMatchSnapshot();
    done();
  }
});

it('should display the configuration form when the article details are loaded', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    name: 'Some article name',
  }));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(toJson(page.find(BareArticleManagementPage))).toMatchSnapshot();
    done();
  }
});

it('should link the labels to their respective form fields', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    name: 'Arbitrary article name',
  }));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();

    const controls = page.find('div.field');
    expect(controls.at(0).find('label').prop('htmlFor')).toBeDefined();
    expect(controls.at(0).find('label').prop('htmlFor')).toBe(controls.at(0).find('input').prop('id'));
    expect(controls.at(1).find('label').prop('htmlFor')).toBeDefined();
    expect(controls.at(1).find('label').prop('htmlFor')).toBe(controls.at(1).find('textarea').prop('id'));
    done();
  }
});

it('should pass previously uploaded files on to the FileManager', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    ...mockScholarlyArticle,
    associatedMedia: [
      { name: 'some_filename', contentUrl: 'some_url' },
    ],
  }));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('[href="some_url"]')).toExist();
    expect(page.find('[href="some_url"]').text()).toBe('some_filename');
    done();
  }
});

it('should submit the form field values when updating the form', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('.details-update-form [name="description"]').first()
      .simulate('change', { target: { name: 'description', value: 'Some description' } });
    page.find('.details-update-form [name="name"]').first()
      .simulate('change', { target: { name: 'name', value: 'Some name' } });
    page.find('.details-update-form').first().simulate('submit');
    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(mockedPeriodicalService.updateScholarlyArticle.mock.calls.length).toBe(1);
    expect(mockedPeriodicalService.updateScholarlyArticle.mock.calls[0][1]).toEqual({
      description: 'Some description',
      name: 'Some name',
    });
    done();
  }
});

it('should display a success message when the article details were successfully saved', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.updateScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    result: {
      name: 'Arbitrary name',
    },
  }));
  const updateSuccessDispatch = jest.fn();

  const page = initialisePage({ onUpdate: updateSuccessDispatch });

  setImmediate(() => {
    page.update();
    page.find('.details-update-form').first().simulate('submit');
    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(updateSuccessDispatch.mock.calls.length).toBe(1);
    expect(updateSuccessDispatch.mock.calls[0][0]).toMatchSnapshot();

    done();
  }
});

it('should send an analytics event when the user is changing the article details', (done) => {
  const mockedReactGa = require.requireMock('react-ga');
  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('.details-update-form').first().simulate('submit');

    expect(mockedReactGa.event.mock.calls.length).toBe(1);
    expect(mockedReactGa.event.mock.calls[0][0]).toEqual({
      action: 'Update details',
      category: 'Article Management',
    });

    done();
  });
});

it('should display an error message when there was an error saving the article details', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  const updatePromise = Promise.reject('Arbitrary error');
  updatePromise.catch(() => '');
  mockedPeriodicalService.updateScholarlyArticle.mockReturnValueOnce(updatePromise);

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('.details-update-form').first().simulate('submit');
    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(page.find('.is-warning').text())
      .toMatch('There was a problem updating the article details, please try again.');
    done();
  }
});

it('should not allow submission when the article does not have its name and abstract set yet', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    ...mockScholarlyArticle,
    description: undefined,
    name: undefined,
  }));

  const page = initialisePage({ orcid: 'arbitrary_orcid' });

  setImmediate(() => {
    page.update();
    expect(page.find('.publication-form #detailsTodo').hasClass('publicationProgress__todo--done')).toBe(false);
    expect(page.find('.publication-form [type="submit"]').first().prop('disabled')).toBe(true);

    done();
  });
});

it('should not allow submission when the article does not have an uploaded PDF yet', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    ...mockScholarlyArticle,
    associatedMedia: undefined,
  }));

  const page = initialisePage({ orcid: 'arbitrary_orcid' });

  setImmediate(() => {
    page.update();
    expect(page.find('.publication-form #uploadTodo').hasClass('publicationProgress__todo--done')).toBe(false);
    expect(page.find('.publication-form [type="submit"]').first().prop('disabled')).toBe(true);

    done();
  });
});

it('should mark the upload task as done when the user uploads a file', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve(mockScholarlyArticle));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('FileManager').prop('onUpload')([ { name: 'arbitrary_filename', contentUrl: 'arbitrary_url' } ]);
    page.update();

    expect(page.find('.publication-form #uploadTodo').hasClass('publicationProgress__todo--done')).toBe(true);

    done();
  });
});

it('should show a notification when the user successfully uploads a file', (done) => {
  const mockUpdateCallback = jest.fn();
  const page = initialisePage({ onUpdate: mockUpdateCallback });

  setImmediate(() => {
    page.update();
    page.find('FileManager').prop('onUpload')([ { name: 'arbitrary_filename', contentUrl: 'arbitrary_url' } ]);

    expect(mockUpdateCallback.mock.calls.length).toBe(1);
    expect(mockUpdateCallback.mock.calls[0][0]).toEqual(<span>Document uploaded successfully!</span>);

    done();
  });
});

it('should send an analytics event when the user uploads a file', (done) => {
  const mockedReactGa = require.requireMock('react-ga');
  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('FileManager').prop('onUpload')([ { name: 'arbitrary_filename', contentUrl: 'arbitrary_url' } ]);

    expect(mockedReactGa.event.mock.calls.length).toBe(1);
    expect(mockedReactGa.event.mock.calls[0][0]).toEqual({
      action: 'Upload',
      category: 'Article Management',
    });

    done();
  });
});

it('should not allow submission when the user has not linked their ORCID yet', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve(mockScholarlyArticle));

  const page = initialisePage({ orcid: undefined });

  setImmediate(() => {
    page.update();
    expect(page.find('.publication-form #accountTodo').hasClass('publicationProgress__todo--done')).toBe(false);
    expect(page.find('.publication-form [type="submit"]').first().prop('disabled')).toBe(true);

    done();
  });
});

it('should allow submission when the user has completed all TODOs', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve(mockScholarlyArticle));

  const page = initialisePage({ orcid: 'arbitrary_orcid' });

  setImmediate(() => {
    page.update();
    expect(page.find('.publication-form #detailsTodo').hasClass('publicationProgress__todo--done')).toBe(true);
    expect(page.find('.publication-form #uploadTodo').hasClass('publicationProgress__todo--done')).toBe(true);
    expect(page.find('.publication-form #accountTodo').hasClass('publicationProgress__todo--done')).toBe(true);
    expect(page.find('.publication-form [type="submit"]').first().prop('disabled')).toBe(false);

    done();
  });
});

it('should indicate when a submission is in progress', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve(mockScholarlyArticle));
  mockedPeriodicalService.submitScholarlyArticle.mockReturnValueOnce(new Promise(() => undefined));

  const page = initialisePage({ orcid: 'arbitrary_orcid' });

  setImmediate(() => {
    page.update();
    page.find('.publication-form').simulate('submit');
    expect(page.find('.publication-form [type="submit"]').first().hasClass('is-loading')).toBe(true);

    done();
  });
});

it('should send an analytics event when the user submits their article', (done) => {
  const mockedReactGa = require.requireMock('react-ga');
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve(mockScholarlyArticle));
  const page = initialisePage({ orcid: 'arbitrary_orcid' });

  setImmediate(() => {
    page.update();
    page.find('.publication-form').simulate('submit');

    expect(mockedReactGa.event.mock.calls.length).toBe(1);
    expect(mockedReactGa.event.mock.calls[0][0]).toEqual({
      action: 'Submit',
      category: 'Article Management',
    });

    done();
  });
});

it('should publish an article without a journal when it was not linked to one', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    ...mockScholarlyArticle,
    isPartOf: undefined,
  }));
  mockedPeriodicalService.publishScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    result: { datePublished: 'Arbitrary date' },
  }));
  const updateSuccessDispatch = jest.fn();

  const page = initialisePage({ orcid: 'arbitrary_orcid', onUpdate: updateSuccessDispatch });

  setImmediate(() => {
    page.update();
    page.find('.publication-form').simulate('submit');

    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    expect(mockedPeriodicalService.publishScholarlyArticle.mock.calls.length).toBe(1);
    expect(mockedPeriodicalService.publishScholarlyArticle.mock.calls[0].length).toBe(1);

    done();
  }
});

it('should display a success message when an article was successfully submitted', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve(mockScholarlyArticle));
  mockedPeriodicalService.submitScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    result: { datePublished: 'Arbitrary date' },
  }));
  const updateSuccessDispatch = jest.fn();

  const page = initialisePage({ orcid: 'arbitrary_orcid', onUpdate: updateSuccessDispatch });

  setImmediate(() => {
    page.update();
    page.find('.publication-form').simulate('submit');

    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(updateSuccessDispatch.mock.calls.length).toBe(1);
    expect(updateSuccessDispatch.mock.calls[0][0]).toMatchSnapshot();

    done();
  }
});

it('should display an error message when an article could not be submitted', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve(mockScholarlyArticle));
  const failedPromise = Promise.reject('Arbitrary error');
  failedPromise.catch(() => undefined);
  mockedPeriodicalService.submitScholarlyArticle.mockReturnValueOnce(failedPromise);

  const page = initialisePage({ orcid: 'arbitrary_orcid' });

  setImmediate(() => {
    page.update();
    page.find('.publication-form').simulate('submit');

    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(page.find('.message.is-warning').text())
      .toMatch('There was a problem submitting your article, please try again.');

    done();
  }
});

it('should not show the submission notification when the article was already submitted', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    ...mockScholarlyArticle,
    datePublished: 'Arbitrary date',
  }));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find('.publication-form')).not.toExist();

    done();
  });
});
