jest.mock('../../src/services/fetchResource');
jest.mock('localforage');

import { getAccount, getAccountId, getProfiles, getSession, getToken, verifyOrcid } from '../../src/services/account';

function mockToken(payload: { exp: number; iat: number; } = { exp: 2, iat: 0 }) {
  return `header.${btoa(JSON.stringify(payload))}.signature`;
}

describe('getSession', () => {
  it('should reject when it could not access local storage for reading', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockImplementation(() => Promise.reject('arbitrary error'));

    return expect(getSession()).rejects.toBeDefined();
  });

  it('should return session details from local storage if set', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockImplementationOnce(() => Promise.resolve({ identifier: 'arbitrary id' }));
    mockedLocalforage.getItem.mockImplementationOnce(() => Promise.resolve(null));

    return expect(getSession()).resolves.toEqual({ identifier: 'arbitrary id' });
  });

  it('should include account details when they are also stored in local storage', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockImplementationOnce(() => Promise.resolve({ identifier: 'some id' }));
    mockedLocalforage.getItem.mockImplementationOnce(() => Promise.resolve(
      { identifier: 'some id', orcid: 'some orcid' },
    ));

    return expect(getSession()).resolves.toEqual({
      account: {
        identifier: 'some id',
        orcid: 'some orcid',
      },
      identifier: 'some id',
    });
  });

  it('should not call the API when session details are stored in local storage', (done) => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockImplementation(() => Promise.resolve(
      { session: { identifier: 'arbitrary id' }, jwt: 'arbitrary jwt', refreshToken: 'arbitrary token' },
    ));

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;

    getSession().then(checkCall);

    function checkCall() {
      expect(mockedFetchResource.mock.calls.length).toBe(0);
      done();
    }
  });

  it('should reject when the API returns an error', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockImplementation(() => Promise.resolve(null));

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockImplementation(() => Promise.resolve({ ok: false }));

    return expect(getSession()).rejects.toEqual(new Error('Error fetching session details'));
  });

  it('should reject when it could not access local storage for writing', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockImplementation(() => Promise.resolve(null));

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockImplementation(() => Promise.resolve({
      json: () => ({ identifier: 'arbitrary id' }),
      ok: true,
    }));

    mockedLocalforage.setItem.mockImplementation(() => Promise.reject('arbitrary error'));

    return expect(getSession()).rejects.toBeDefined();
  });

  it('should store fetched session details and its tokens in local storage', (done) => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockImplementation(() => Promise.resolve(null));
    mockedLocalforage.setItem.mockImplementation(() => Promise.resolve());

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockImplementation(() => Promise.resolve({ ok: true, json: () => {
      return {
        session: { identifier: 'arbitrary id' },
        jwt: 'arbitrary JSON Web Token',
        refreshToken: 'arbitrary token',
      };
    } }));

    getSession().then(checkCall);

    function checkCall() {
      expect(mockedLocalforage.setItem.mock.calls.length).toBe(3);
      expect(mockedLocalforage.setItem.mock.calls[0][0]).toBe('session');
      expect(mockedLocalforage.setItem.mock.calls[0][1]).toEqual({ identifier: 'arbitrary id' });
      expect(mockedLocalforage.setItem.mock.calls[1][0]).toBe('session_jwt');
      expect(mockedLocalforage.setItem.mock.calls[1][1]).toEqual('arbitrary JSON Web Token');
      expect(mockedLocalforage.setItem.mock.calls[2][0]).toBe('session_refreshToken');
      expect(mockedLocalforage.setItem.mock.calls[2][1]).toEqual('arbitrary token');
      done();
    }
  });

  it('should return fetched session details as stored in local storage', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockImplementation(() => Promise.resolve(null));
    mockedLocalforage.setItem.mockImplementation(() => Promise.resolve({ identifier: 'arbitrary id' }));

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockImplementation(() => Promise.resolve({ ok: true, json: () => 'Arbitrary response' }));

    return expect(getSession()).resolves.toEqual({ identifier: 'arbitrary id' });
  });
});

describe('getToken', () => {
  it('should return a stored token if it hasn\'t expired yet', () => {
    const mockedLocalforage = require.requireMock('localforage');
    const mockedToken = mockToken({ exp: 2, iat: 0 });
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve(mockedToken));
    Date.now = jest.fn().mockReturnValueOnce(1000);

    return expect(getToken('arbitrary user')).resolves.toEqual(mockedToken);
  });

  it('should fetch a new token if the stored one is expired', (done) => {
    const mockedLocalforage = require.requireMock('localforage');
    const mockedToken = mockToken({ exp: 2, iat: 0 });
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve(mockedToken));
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve('arbitrary refresh token'));
    Date.now = jest.fn().mockReturnValueOnce(3000);
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({ ok: true, json: () => 'Arbitrary response' }));

    const token = getToken('arbitrary user');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][1].body)
        .toBe(JSON.stringify({ refreshToken: 'arbitrary refresh token' }));
      done();
    });
  });

  it('should return a new token if none is stored yet', (done) => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve(null));
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve('arbitrary refresh token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({ ok: true, json: () => 'Arbitrary response' }));

    const token = getToken('arbitrary user');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][1].body)
        .toBe(JSON.stringify({ refreshToken: 'arbitrary refresh token' }));
      done();
    });
  });

  it('should store fetched tokens in local storage', (done) => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve(null));
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve('arbitrary refresh token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => ({ jwt: 'Arbitrary response' }),
      ok: true,
    }));

    const token = getToken('arbitrary user');

    setImmediate(() => {
      expect(mockedLocalforage.setItem.mock.calls.length).toBe(1);
      expect(mockedLocalforage.setItem.mock.calls[0][0]).toBe('session_jwt');
      expect(mockedLocalforage.setItem.mock.calls[0][1]).toBe('Arbitrary response');
      done();
    });
  });

  it('should return the stored token', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve(null));
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve('arbitrary refresh token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({ ok: true, json: () => 'Arbitrary response' }));
    mockedLocalforage.setItem.mockReturnValueOnce('Arbitrary stored token');

    return expect(getToken('arbitrary user')).resolves.toBe('Arbitrary stored token');
  });

  it('should error when no refresh token is stored', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve(null));
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve(null));

    return expect(getToken('arbitrary user')).rejects.toBeDefined();
  });

});

describe('getAccount', () => {
  it('should return whatever is stored in local storage', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve('Stored ORCID'));

    return expect(getAccount()).resolves.toBe('Stored ORCID');
  });

  it('should return null when nothing is stored in local storage', () => {
    const mockedLocalforage = require.requireMock('localforage');
    mockedLocalforage.getItem.mockReturnValueOnce(Promise.resolve(null));

    return expect(getAccount()).resolves.toBe(null);
  });
});

describe('verifyOrcid', () => {
  it('should reject with an error if the API returns an error', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({ ok: false }));

    return expect(verifyOrcid('arbitrary_session', 'arbitrary token', '123456')).rejects
      .toEqual(new Error('There was an error verifying your ORCID.'));
  });

  it('should store the returned JWT in local storage', (done) => {
    const mockedLocalforage = require.requireMock('localforage');
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      ok: true,
      json: () => Promise.resolve({ jwt: 'Some JWT', account: { identifier: 'Some ID', orcid: 'Some ORCID' } }),
    }));

    verifyOrcid('arbitrary_session', 'arbitrary token', '123456');

    setImmediate(() => {
      expect(mockedLocalforage.setItem.mock.calls.length).toBe(2);
      expect(mockedLocalforage.setItem.mock.calls[0][0]).toBe('session_jwt');
      expect(mockedLocalforage.setItem.mock.calls[0][1]).toBe('Some JWT');
      expect(mockedLocalforage.setItem.mock.calls[1][0]).toBe('account');
      expect(mockedLocalforage.setItem.mock.calls[1][1])
        .toEqual({ identifier: 'Some ID', orcid: 'Some ORCID' });
      done();
    });
  });
});

describe('getAccountId', () => {
  it('should reject with an error if the API returns an error', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.reject(new Error('Arbitrary error')));

    return expect(getAccountId('arbitrary orcid')).rejects
      .toEqual(new Error('Arbitrary error'));
  });

  it('should return the fetched ID', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => ({ identifier: 'some-id' }),
    }));

    return expect(getAccountId('arbitrary orcid')).resolves
      .toBe('some-id');
  });
});

describe('getProfiles', () => {
  it('should reject with an error if the API returns an error', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.reject(new Error('Arbitrary error')));

    return expect(getProfiles([ 'arbitrary_profile' ])).rejects
      .toEqual(new Error('Arbitrary error'));
  });

  it('should pass multiple identifiers through to the URL', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;

    getProfiles([ 'account_id_1', 'account_id_2' ]);

    expect(mockedFetchResource.mock.calls.length).toBe(1);
    expect(mockedFetchResource.mock.calls[0][0]).toMatch('account_id_1+account_id_2');
  });
});
