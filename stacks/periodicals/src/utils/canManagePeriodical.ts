import { Session } from '../../../../lib/interfaces/Session';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';
import { isPeriodicalOwner } from '../../../../lib/utils/periodicals';
import { fetchPeriodical } from '../services/periodical';

export async function canManagePeriodical(database: Database, periodicalId: string, session: Session) {
  const storedPeriodical =
    await fetchPeriodical(database, periodicalId, session);

  const periodicalCreator = storedPeriodical.creator || undefined;

  return (
    storedPeriodical.creatorSessionId === session.identifier
    || (typeof periodicalCreator !== 'undefined' && isPeriodicalOwner({ creator: periodicalCreator }, session))
  );
}
